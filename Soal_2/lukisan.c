#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <zip.h>
#include <sys/stat.h>

void bikinfolder(char *nmfolder) {
    struct tm *statuswaktu;
    char timestamp[20];
    time_t waktuskrg;

    time(&waktuskrg);
    statuswaktu = waktulokal(&waktuskrg);

    strftime(timestamp, sizeof(timestamp), "[%Y-%m-%d_%H:%M:%S]", statuswaktu);

    mkdir(timestamp, 0777);

}

time_t ubahepoch(const char *nmfolder) {
    struct tm waktu_stat;
    memset(&waktu_stat, 0, sizeof(struct tm));

    sscanf(nmfolder, "[%d-%d-%d_%d:%d:%d]",
           &waktu_stat.tm_thn, &waktu_stat.tm_bln, &waktu_stat.tm_hari,
           &waktu_stat.tm_jam, &waktu_stat.tm_mnt, &waktu_stat.tm_dtk);

    waktu_stat.tm_thn -= 1900;
    waktu_stat.tm_bln--;

    return mktime(&waktu_stat);
}

int masukzip(const char *namazip, const char *nmfolder) {
    int err;
    struct zip *za;
    za = zip_open(namazip, ZIP_CREATE | ZIP_TRUNCATE, &err);
    if (!za) {
        fprintf(stderr, "tidak dapat membuka zip: %s\n", zip_strerror(za));
        return -1;
    }

    DIR *dir;
    struct dirent *entry;

    dir = opendir(nmfolder);
    if (dir == NULL) {
        perror("tidak dapat membuka folder");
        zip_close(za);
         return -1;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { 
            char filePath[200];
            snprintf(filePath, sizeof(filePath), "%s/%s", nmfolder, entry->d_name);
            if (zip_file_add(za, entry->d_name, filePath, ZIP_FL_ENC_UTF_8) < 0) {
                fprintf(stderr, "tidak dapat add file ke dalam ZIP: %s\n", zip_strerror(za));
            }
        }
    }

    int hapusfolder(const char *nmfolder) {
    if (rmdir(nmfolder) == 0) {
        return 0; 
    } else {
        perror("Error");
        return -1;
    }
}

    closedir(dir);

    if (zip_close(za) < 0) {
        fprintf(stderr, "tidak dapat menutup ZIP: %s\n", zip_strerror(za));
        return -1;
    }

    return 0;
}
int main() {
    struct tm *statuswaktu;
    char nmfolder[20];

    while (1) {
        bikinfolder(nmfolder);

        time_t epoch_unix = ubahepoch(nmfolder);
        statuswaktu = waktulokal(&epoch_unix);

        for (int i = 0; i < 15; i++) {
            time_t current_time = time(NULL);
            struct tm *statuswaktu = waktulokal(&current_time);

            int width = (epoch_unix % 1000) + 50;
            int height = (epoch_unix % 1000) + 50;

            char namapcture[50];  
            snprintf(namapcture, sizeof(namapcture), "[%04d-%02d-%02d_%02d:%02d:%02d].jpg",
            statuswaktu->tm_thn + 1900, statuswaktu->tm_bln + 1, statuswaktu->tm_hari,
            statuswaktu->tm_jam, statuswaktu->tm_mnt, statuswaktu->tm_dtk);

            char url[100];
            snprintf(url, sizeof(url), "https://source.unsplash.com/%dx%d", width, height);

            pid_t child_pid = fork();
            if (child_pid == 0) {

                char pathpcture[200];
                snprintf(pathpcture, sizeof(pathpcture), "./%s/%s", nmfolder, namapcture);

                execlp("wget", "wget", "-O", pathpcture, url, NULL);

                perror("execlp");
                exit(EXIT_FAILURE);
            } else if (child_pid > 0) {
                wait(NULL);
            } else {
                perror("fork");
                exit(EXIT_FAILURE);
            }
            sleep(5);
        }
        sleep(30);

        char namazip[50];
        snprintf(namazip, sizeof(namazip), "%s.zip", nmfolder);

        if (masukzip(namazip, nmfolder))
    }
    return 0;
}

