# sisop-praktikum-modul-2-2023-MH-IT20
Laporan pengerjaan soal shift modul 2 Praktikum Sistem Operasi 2023 Kelompok IT20

## Anggota Kelompok:
1. M. Januar Eko Wicaksono  -    5027221006
2. Rizki Ramadhani	        -    5027221013
3. Khansa Adia Rahma 		-    5027221071

## Soal 1

### Study case soal 1
---
John adalah seorang mahasiswa biasa. Pada tahun ke-dua kuliahnya, dia merasa bahwa dia telah menyianyiakan waktu kuliahnya selama ini. Selama mengerjakan tugas, John selalu menggunakan bantuan ChatGPT dan tidak pernah mempelajari apapun dari hal tersebut. Untuk itu, pada soal kali ini John bertekad untuk tidak menggunakan ChatGPT dan mencoba menyelesaikan tugasnya dengan tangan dan pikirannya sendiri. Melihat tekad yang kuat dari John, Mark, dosen yang mengajar John, ingin membantunya belajar dengan memberikan sebuah ujian. Sebelum memberikan ujian pada John, Mark berpesan bahwa John harus bersungguh-sungguh dalam mengerjakan ujian, fokus untuk belajar, dan tidak perlu khawatir akan nilai yang diberikan. Mark memberikan ujian pada John untuk membuatkannya sebuah program `cleaner` sederhana dengan ketentuan berikut :

### Problem
---
a. Program tersebut menerima input path dengan menggunakan “argv”.

b. Program tersebut bertugas untuk menghapus file yang didalamnya terdapat string "SUSPICIOUS" pada direktori yang telah diinputkan oleh user.

c. Program tersebut harus berjalan secara daemon.

d. Program tersebut akan terus berjalan di background dengan jeda 30 detik.

e. Dalam pembuatan program tersebut, tidak diperbolehkan menggunakan fungsi `system()`.

f. Setiap kali program tersebut menghapus sebuah file, maka akan dicatat pada file 'cleaner.log' yang ada pada direktori home user dengan format seperti berikut : “[YYYY-mm-dd HH:MM:SS] '<absolute_path_to_file>' has been removed.”.

  - Example usage:
    > ./cleaner '/home/user/cleaner'
  - Example cleaner.log:
    > [2023-10-02 18:54:20] '/home/user/cleaner/is.txt' has been removed. <br>
    > [2023-10-02 18:54:20] '/home/user/cleaner/that.txt' has been removed. <br>
    > [2023-10-02 18:54:50] '/home/user/cleaner/who.txt' has been removed. <br>
  - Example valid sample file:
    > LJuUHmAnVLLCMRhLTcqy
 <br>bBp**SUSPICIOUS**agQKmLA <br>
    > BitSuQNHSLmZDvEcvbGc <br>
  - Example valid sample file:
    > LJuUHmAnVLLCMRhLTcqy
 <br>TFvefehhpWDCbkdirmlh <br>
    > BitSuQNHSLmZDvEcvbGc <br>

### Solution
---

### Kendala (Error)
---

### Revisi
---

### Hasil
---


## Soal 2

### Study case soal 2
---
QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster foto roster Cleveland Cavaliers tahun 2016. Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar - gambar pemain tersebut. Sebagai teman baiknya, bantu QQ untuk mencarikan foto foto yang dibutuhkan QQ dengan ketentuan sebagai berikut:

### Problem
---
a. Pertama, buatlah program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”.

b. Program akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip” ke dalam folder players yang telah dibuat. Lalu hapus file zip tersebut agar tidak memenuhi komputer QQ.

c. Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.

d. Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).

e. Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
- PG: {jumlah pemain}
- SG: {jumlah pemain}
- SF: {jumlah pemain}
- PF: {jumlah pemain}
- C: {jumlah pemain}

f. Ia ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7, dengan membuat folder “clutch”, yang di dalamnya berisi foto pemain yang bersangkutan.

g. Ia merasa kurang lengkap jika tidak memajang foto pemain yang melakukan The Block pada ajang yang sama, Maka dari itu ditaruhlah foto pemain tersebut di folder “clutch” yang sama.

Note:
- Format nama file yang akan diunduh dalam zip berupa [tim]-[posisi]-[nama].png
- Tidak boleh menggunakan system(), Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- 2 poin soal terakhir dilakukan setelah proses kategorisasi selesai.

### Solution
---

a. Pertama kali, kita disuruh untuk membuat file dengn nama Cavs.c yang merupakan program C untuk menjalankan perintah yang diperintahkan. Disini saya menggunakan IDE VsCode. Jadi tinggal menambah file didalam VsCode. Atau jika menggunakan Command Line dengan cara:
```c
nano cavs.c
```

b. kemudian kita download file .zip pada drive link dibawah ini:
```c
https://drive.google.com/file/d/1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK
```

Berikut adalah function-function yang digunakan untuk menjalankan program ini:

Function-function yang harus mengakses command dari system linux akan menggunakan fork untuk men-spawning proses, tiap-tiap command yang akan dieksekusi akan dijalankan di oleh proses child dan jika ternyata proses parent yang dijalankan duluan, makan akan mengeksekusi wait(Null). 

Dengan menggabungkan fork dan exec, kita dapat melakukan dua atau lebih tasks secara bersamaan.

Berikut merupakan program untuk mendowload file players.zip dan sekaligus membuat folder "players" tersebut:


```c
void downloadDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        
        char* const wgetCmd[] = {
            "wget", "https://drive.google.com/uc?export=download&id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", 
            NULL
        };
        
        execvp("wget", wgetCmd);
        
        perror("Execvp failed");
        exit(1);
    } else {
        
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Download berhasil.\n");
        } else {
            printf("Download gagal.\n");
        }
    }
}

```
Setelah itu, di unzip file palyers.zip pada folder players yang sudah dibuat. Dengan menggunakan program fork() dan excevp dibawah ini:

```c
void extractDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        char* const unzipCmd[] = {
            "unzip", "players.zip", "-d", "players", 
            NULL
        };
        
        execvp("unzip", unzipCmd);
        
        perror("Execvp failed");
        exit(1);
    } else {
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Ekstraksi berhasil.\n");
        } else {
            printf("Ekstraksi gagal.\n");
        }
    }
}

void deleteZipDriveNBA() {
    pid_t child_pid = fork();
    
    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }
    
    if (child_pid == 0) {
        char* const rmCmd[] = {
            "rm", "players.zip", 
            NULL
        };
        
        execvp("rm", rmCmd);
        perror("Execvp failed");
        exit(1);
    } else {
        int status;
        waitpid(child_pid, &status, 0);
        
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("File ZIP berhasil dihapus.\n");
        } else {
            printf("File ZIP gagal dihapus.\n");
        }
    }
}


```
c. Berikut merupakan function fork() dan execvp untuk menghapus players yang bukan merupakan Clevelan Cavaliers:
```c
int isCavsPlayer(const char* filename) {
    return strstr(filename, "Cavaliers") != NULL;
}
```

dan berikut function void nya:
```c
void categorizePlayersCavs() {
    char* positions[] = {"PG", "SG", "SF", "PF", "C"};
    int playerCounts[5] = {0};

    FILE* output = fopen("Formasi.txt", "w");
    if (output == NULL) {
        perror("Failed to open Formasi.txt for categorize");
        exit(1);
    }

    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    // Fungsi untuk menghapus pemain yang bukan dari Cleveland Cavaliers
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            if (!isCavsPlayer(entry->d_name)) {
                char filePath[512]; 
                snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);
                // Menghapus file yang tidak termasuk dalam pemain Cleveland Cavaliers
                remove(filePath);  
            }
        }
    }
    
    closedir(dir);
```

d. Kemudian berikut merupakan function untuk menghitung pemain Cleveland Cavaliers di dalam file formasi.txt. secara otomatis pada funtion berikut:

```c
 // Inisialisasi hitungan pemain untuk setiap posisi
    int Count_PG = 0, Count_SG = 0, Count_SF = 0, Count_PF = 0, Count_C = 0;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    // Fungsi untuk menghitung pemain Cavaliers berdasarkan posisi "PG", "SG", "SF", "PF", "C"
    while ((entry = readdir(dir)) != NULL) {
        if (strstr(entry->d_name, "Cavaliers") != NULL && strstr(entry->d_name, ".png") != NULL) {
            char filepath[512];
            char PosisiPlayers[10];
            snprintf(filepath, sizeof(filepath), "players/%s", entry->d_name);
            sscanf(entry->d_name, "Cavaliers-%2s", PosisiPlayers);
            char Categorize_folder[32];
            snprintf(Categorize_folder, sizeof(Categorize_folder), "players/%s", PosisiPlayers);

            struct stat st = {0};
            if (stat(Categorize_folder, &st) == -1) {
                mkdir(Categorize_folder, 0777);
            }

            char New_Folders[512];
            snprintf(New_Folders, sizeof(New_Folders), "%s/%s", Categorize_folder, entry->d_name);
            rename(filepath, New_Folders);

            if (strcmp(PosisiPlayers, "PG") == 0) Count_PG++;
            else if (strcmp(PosisiPlayers, "SG") == 0) Count_SG++;
            else if (strcmp(PosisiPlayers, "SF") == 0) Count_SF++;
            else if (strcmp(PosisiPlayers, "PF") == 0) Count_PF++;
            else if (strcmp(PosisiPlayers, "C-") == 0) Count_C++;
        }
    }
    closedir(dir);
    // Menulis informasi hitungan players ke dalam Formasi.txt
    fprintf(output, "PG: %d\n", Count_PG);
    fprintf(output, "SG: %d\n", Count_SG);
    fprintf(output, "SF: %d\n", Count_SF);
    fprintf(output, "PF: %d\n", Count_PF);
    fprintf(output, "C: %d\n", Count_C);

    fclose(output);
}
```

Dengan hasil output dalam file formasi.txt yaitu:
```c
PG: 4
SG: 3
SF: 2
PF: 1
C: 3
```
d. Setelah itu, pada soal disuruh mengkategorikan players sesuai dengan posisi masing" yaitu "PG", "SG", "SF", "PF", "C". Pertama membuat folder kategori sesuai posisi tersebut dengan cara:
```c
//Fungsi untuk mengkategorikan players sesuai dengan posisi masing"
void createCategoryFolderPlayersCavs() {
    
    mkdir("players/PG", 0755);
    mkdir("players/SG", 0755);
    mkdir("players/SF", 0755);
    mkdir("players/PF", 0755);
    mkdir("players/C-", 0755);
}

```
Kemudian masukkan foto pemain Cleveland Cavaliers dengan menggunakan program code sebagai berikut:
```c
void categorizePngFilePlayers() {
    char* positions[] = {"PG", "SG", "SF", "PF", "C"};
    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);

            if (isCavsPlayer(entry->d_name)) {
                for (int i = 0; i < 5; i++) {
                    if (strstr(entry->d_name, positions[i]) != NULL && strstr(entry->d_name, ".") != NULL) {
                        char destPath[512];
                        snprintf(destPath, sizeof(destPath), "players/%s/%s", positions[i], entry->d_name);
                        rename(filePath, destPath);
                    }
                }
            }
        }
    }

    closedir(dir);
}
```

f. Pada 2 soal terakhir diperintahkan untuk memajang sebuah foto yang termasuk " pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7" dan "pemain yang melakukan The Block pada ajang yang sama" di dalam folder "clutch".

Pertama membuat terlebih dahulu folder "clutch" dengan cara:
```c
void createClutchFolder() {
    mkdir("clutch", 0755);
}
```

Setelah itu kita pindahkan pemain yang bernama "Kyrie-Irving" yang merupakan 
pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7" dan "LeBron-James" yang merupakan "pemain yang melakukan The Block pada ajang yang sama" Dengan program berikut:
```c
void movePlayersCavsToClutchFolder() {
    DIR* dir;
    struct dirent* entry;

    dir = opendir("players");
    if (dir == NULL) {
        perror("Failed to open players directory");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char filePath[512];
            snprintf(filePath, sizeof(filePath), "players/%s", entry->d_name);

            if (isCavsPlayer(entry->d_name)) {
                if (strstr(entry->d_name, "LeBron-James") != NULL || strstr(entry->d_name, "Kyrie-Irving") != NULL) {
                    char destPath[512];
                    snprintf(destPath, sizeof(destPath), "clutch/%s", entry->d_name);
                    rename(filePath, destPath);
                }
            }
        }
    }

    closedir(dir);
}
```

Setelah semua selesai jadikan satu function void ke dalam "int main()" untuk melakukan perintah program yang diperintahkan, untuk menghasilkan output yang diinginkan dengan cara sebagai berikut:
```c
int main() {
    pid_t child_pid;

    child_pid = fork();

    if (child_pid < 0) {
        perror("Fork failed");
        exit(1);
    }

    if (child_pid == 0) {
        downloadDriveNBA();
        extractDriveNBA();
        deleteZipDriveNBA();
        categorizePlayersCavs();
    createCategoryFolderPlayersCavs();
        createClutchFolder();
        movePlayersCavsToClutchFolder();
        sleep(2);
categorizePngFilePlayers();
        printf("Selesai\n");
    } else {
        wait(NULL);
    }

    return 0;
}
```
Jadikan satu program-program diatas pada satu file yang bernama "cavs.c" untuk menjalankan semua perintahnya kemudian dijalankan pada VsCode dengan cara:
```c
Ctrl+Alt+n
```

### Kendala (Error)
---
1. Tidak bisa memasukkan foto "LeBron-James.png" dan foto "Kyrie-Irving.png" ke dalam folder "clutch".


### Revisi
---
---
> Program yang ditambahkan setelah demo soal shift 2 dilakukan

1. Sudah bisa memindahkan foto "Kyrie-Irving.png" dan "Lebron-James.pnh" pada folder "clutch".

### Hasil
---
1. Result Program Categorize_Players_Cavaliers

![Hasil Categorize_Plaers_Cavs](img/soal_2/Hasil_Categorize_Folder.png)

2. Result Formasi.txt
![Hasil Formasi.txt](img/soal_2/Hasil_Formasi.txt.png)

3. Result Clutch_Folder 
![Hasil Clutch_Folder](img/soal_2/Hasil_Clutch_Folder.png)

## Soal 3

### Study case soal 3
---
Albedo adalah seorang seniman terkenal dari Mondstadt. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini Albedo sedang menghadapi creativity block. Sebagai teman berkebangsaan dari Fontaine yang jago sisop, bantu Albedo untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

### Problem
---
a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip, format nama [YYYY-mm-dd_HH:mm:ss].zip tanpa “[]”).

d. Karena takut program tersebut lepas kendali, Albedo ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Note :
- Tidak boleh menggunakan system().
- Proses berjalan secara daemon.
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping).

### Solution
---
a.membuat folder dengan nama timestamp dalam format [YYYY-MM-dd_HH:mm:ss]. Ini mencakup pengambilan waktu saat ini, konversi waktu ke dalam struktur tm, pembentukan nama folder, dan pembuatan folder dengan hak akses yang sesuai.

```c
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <zip.h>
#include <sys/stat.h>

void bikinfolder(char *nmfolder) {
    struct tm *statuswaktu;
    char timestamp[20];
    time_t waktuskrg;

    time(&waktuskrg);
    statuswaktu = waktulokal(&waktuskrg);

    strftime(timestamp, sizeof(timestamp), "[%Y-%m-%d_%H:%M:%S]", statuswaktu);

    mkdir(timestamp, 0777);

}
```

b. Mengonversi nama folder timestamp ke dalam nilai epoch Unix. Ini melibatkan ekstraksi tahun, bulan, hari, jam, menit, dan detik dari nama folder, dilanjutkan dengan penyesuaian tahun dan bulan yang dimulai dari nilai tertentu.

```c
time_t ubahepoch(const char *nmfolder) {
    struct tm waktu_stat;
    memset(&waktu_stat, 0, sizeof(struct tm));

    sscanf(nmfolder, "[%d-%d-%d_%d:%d:%d]",
           &waktu_stat.tm_thn, &waktu_stat.tm_bln, &waktu_stat.tm_hari,
           &waktu_stat.tm_jam, &waktu_stat.tm_mnt, &waktu_stat.tm_dtk);

    waktu_stat.tm_thn -= 1900;
    waktu_stat.tm_bln--;

    return mktime(&waktu_stat);
}
```

c. membuat file ZIP dan menambahkan file-file dari folder ke dalamnya. Ini melibatkan pembukaan file ZIP, pembukaan direktori untuk membaca file-filenya, dan penambahan file ke dalam ZIP. Juga, penanganan kesalahan saat pembukaan ZIP atau direktori gagal.

```c
int masukzip(const char *namazip, const char *nmfolder) {
    int err;
    struct zip *za;
    za = zip_open(namazip, ZIP_CREATE | ZIP_TRUNCATE, &err);
    if (!za) {
        fprintf(stderr, "tidak dapat membuka zip: %s\n", zip_strerror(za));
        return -1;
    }

    DIR *dir;
    struct dirent *entry;

    dir = opendir(nmfolder);
    if (dir == NULL) {
        perror("tidak dapat membuka folder");
        zip_close(za);
         return -1;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { 
            char filePath[200];
            snprintf(filePath, sizeof(filePath), "%s/%s", nmfolder, entry->d_name);
            if (zip_file_add(za, entry->d_name, filePath, ZIP_FL_ENC_UTF_8) < 0) {
                fprintf(stderr, "tidak dapat add file ke dalam ZIP: %s\n", zip_strerror(za));
            }
        }
    }

    int hapusfolder(const char *nmfolder) {
    if (rmdir(nmfolder) == 0) {
        return 0; 
    } else {
        perror("Error");
        return -1;
    }
}

    closedir(dir);

    if (zip_close(za) < 0) {
        fprintf(stderr, "tidak dapat menutup ZIP: %s\n", zip_strerror(za));
        return -1;
    }

    return 0;
}
```

d. fungsi main terdapat loop utama yang mencakup pembuatan folder timestamp, pengunduhan gambar, dan pembuatan file ZIP. Program berjalan dalam loop tak berhenti (while (1)) untuk terus-menerus menjalankan tugasnya.

```c
int main() {
    struct tm *statuswaktu;
    char nmfolder[20];

    while (1) {
        bikinfolder(nmfolder);

        time_t epoch_unix = ubahepoch(nmfolder);
        statuswaktu = waktulokal(&epoch_unix);

        for (int i = 0; i < 15; i++) {
            time_t current_time = time(NULL);
            struct tm *statuswaktu = waktulokal(&current_time);

            int width = (epoch_unix % 1000) + 50;
            int height = (epoch_unix % 1000) + 50;

            char namapcture[50];  
            snprintf(namapcture, sizeof(namapcture), "[%04d-%02d-%02d_%02d:%02d:%02d].jpg",
            statuswaktu->tm_thn + 1900, statuswaktu->tm_bln + 1, statuswaktu->tm_hari,
            statuswaktu->tm_jam, statuswaktu->tm_mnt, statuswaktu->tm_dtk);

            char url[100];
            snprintf(url, sizeof(url), "https://source.unsplash.com/%dx%d", width, height);

            pid_t child_pid = fork();
            if (child_pid == 0) {

                char pathpcture[200];
                snprintf(pathpcture, sizeof(pathpcture), "./%s/%s", nmfolder, namapcture);

                execlp("wget", "wget", "-O", pathpcture, url, NULL);

                perror("execlp");
                exit(EXIT_FAILURE);
            } else if (child_pid > 0) {
                wait(NULL);
            } else {
                perror("fork");
                exit(EXIT_FAILURE);
            }
            sleep(5);
        }
        sleep(30);

        char namazip[50];
        snprintf(namazip, sizeof(namazip), "%s.zip", nmfolder);

        if (masukzip(namazip, nmfolder))
    }
    return 0;
}
```

### Kendala (Error)
---
Terdapat error saat proses zip folder, selain itu kesalahan dalam pemanggilan fungsi localtime sehingga terjadi error dan kesalahan dalam penggunaan variable untuk timestamp.

### Revisi
---
a. Menerima dua argumen baris perintah (argc adalah jumlah argumen, argv adalah array argumen). Program membuka file "kill.sh" dan menuliskan perintah shell ke dalamnya berdasarkan argumen yang diberikan. Jika argumen adalah -a, maka perintah "killall -9 lukisan" ditambahkan ke file. Jika argumen adalah -b, perintah untuk mencari dan menghentikan proses dengan nama "lukisan" ditambahkan. Setelah itu, program mengeksekusi perintah chmod +x kill.sh untuk membuat file "kill.sh" dapat dieksekusi.
Program kemudian menciptakan proses anak chi untuk mengeksekusi perintah chmod +x kill.sh. Jika proses ini berhasil dibuat, maka program akan keluar.
Selanjutnya, program menciptakan proses orangtua par. Jika proses ini berhasil dibuat, maka program akan keluar.
Program mengatur ulang hak akses file dengan umask(0).
Program menciptakan proses daemon dengan menggunakan setsid(), melepaskan diri dari terminal utama, dan menutup file descriptor standar (STDIN, STDOUT, STDERR).

```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <signal.h>

int main(int argc, char** argv) {
    if (argc == 2) {
        FILE* kill_program = fopen("kill.sh", "w");
        fprintf(kill_program, "#!/bin/bash\n");
        if (!strcmp("-a", argv[1])) {
            fprintf(kill_program, "killall -9 lukisan\nrm $0\n");
        } else if (!strcmp("-b", argv[1])) {
            fprintf(kill_program, "kill_parent()\n");
                fprintf(kill_program, "kill ${@: -1}\n");
                fprintf(kill_program, "}\n");
                fprintf(kill_program, "kill_parent $(pidof lukisan)\nrm $0\n");
        }

        fclose(kill_program);

        pid_t chi = fork();
        if (chi < 0){
            exit(EXIT_FAILURE);
        }
        if (chi == 0){
            execlp("bin/chmod", "chmod", "+x", "kill.sh", NULL);
            exit(0);
        }

        pid_t par, sid;

        par = fork();

        if (par < 0) {
            exit(EXIT_FAILURE);
        }
        
        if (par > 0) {
            exit(EXIT_SUCCESS);
        }

        umask(0);

        sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
    }
```

b. Selanjutnya akan memasuki loop utama yang akan berjalan selamanya dan menciptakan proses anak make_folder yang akan digunakan untuk membuat folder dengan timestamp dan melibatkan pengambilan waktu saat ini dan pemformatan waktu dalam format tertentu yang akan digunakan sebagai nama folder. Selanjutnya pada proses anak make_folder mencoba membuat folder dengan nama timestamp menggunakan perintah shell mkdir.
Menggunakan loop untuk mengunduh dan menyimpan gambar. Dimana setiap iterasi loop, akan menciptakan proses anak download_pic yang mengunduh gambar dari URL yang dibuat berdasarkan timestamp saat itu dan menyimpannya.
Setelah 15 gambar selesai diunduh, program menunggu sampai proses-proses anak selesai dengan wait.
Selanjutnya, program menciptakan proses anak zip_program yang digunakan untuk membuat file ZIP yang berisi gambar-gambar yang telah diunduh.
Program kemudian keluar dari proses anak saat selesai.

```c
while (1) {
        pid_t make_folder;
        time_t current = time(NULL);
        time(&current);
        struct tm* time_info;
        time_info = localtime(&current);
        char timeformat[60];
        strftime(timeformat, sizeof(timeformat), "%Y-%m-%d_%H:%M:%S", time_info);
        make_folder = fork();

        if (make_folder == 0) {
            execlp("/bin/mkdir", "mkdir", timeformat, NULL);
            exit(0);
        } else {
            pid_t loop_download = fork();
            if (loop_download == 0) {
                for (int i = 0; i < 15; i++) {
                    pid_t download_pic = fork();

                    if (download_pic == 0) {
                        current = time(NULL);
                        char link[100];
                        sprintf(link, "https://source.unsplash.com/%ldx%ld", (current%1000)+50, (current%1000)+50);

                        time_t time_pic;
                        struct tm *time_now_pic;
                        char timeformat_pic[30];
                        char download_dir[100];

                        time(&time_pic);
                        time_now_pic = localtime(&time_pic);
                        strftime(timeformat_pic, sizeof(timeformat_pic), "%Y-%m-%d_%H:%M:%S", time_now_pic);
                        sprintf(download_dir, "%s/%s.jpeg", timeformat, timeformat_pic);

                        char *arg[] = {"wget", "-q", "-O", download_dir, link, NULL};
                        execv("/bin/wget", arg);
                    }
                    sleep(5);
                }
                int status;
                wait(&status);
                char zname[100];
                sprintf(zname, "%s.zip", timeformat);
                pid_t zip_program = fork();

                if (zip_program == 0) {
                    char *arg[] = {"zip", zname, "-rm", timeformat, NULL};
                    execv("/bin/zip", arg);
                }
                exit(0);
            }
        }
        sleep(30);
    }
}
```

### Hasil
---

## Soal 4

### Study case soal 4
---
Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk meningkatkan kinerja antivirus yang telah dia buat sebelumnya.

### Problem
---
a. Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. Program ini seharusnya dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder quarantine. list dari format ekstensi/tipe file nya bisa didownload di Link Ini , proses mendownload tidak boleh menggunakan system()
Daftar ekstensi file yang dianggap virus tersimpan dalam file extensions.csv.

b. Ada kejutan di dalam file extensions.csv. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.Setiap kali program mendeteksi file virus, catatlah informasi tersebut di virus.log. Format log harus sesuai dengan:

[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}

Contoh:  [sisopUser][29-09-23:08-59-01] - test.locked - Moved to quarantine

*nama_user: adalah username dari user yang menambahkan file ter-infected

c. Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder sisop_infected setiap detik.

d. Choco juga membutuhkan level-level keamanan antivrus jadi dia membuat 3 level yaitu low, medium ,hard. Argumen tersebut di pakai saat menjalankan antivirus.
- Low: Hanya me-log file yg terdeteksi
- Medium: log dan memindahkan file yang terdeteksi
- Hard: log dan menghapus file yang terdeteksi

ex: **./antivirus -p low**

Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk mengganti level keamanan antivirus dengan mengirim sinyal ke daemon. Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard".

Contoh:
  - Example usage:
    > kill -SIGUSR1 <pid_program>

e. Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.

### Solution
---

### Kendala (Error)
---

### Revisi
---

### Hasil
---

